# B!SON widget

This project provides a simple and minimalistic widget for the B!SON service
that is simple to embed in web pages.

You can include it via:
```
<div id="bison-widget"></div>
<script src="https://service.tib.eu/bison/widget/bison-widget.js" type="text/javascript"></script>
```

or via iframe:

```
<iframe src="https://service.tib.eu/bison/widget/bison_widget.html" height="500" width="500" title="Open access journal recommendations via BISON service"></iframe> 
```


## Development
The widget is written in plain javascript and bundled/transpiled via webpack
and babel.
To compile it yourself run:
```
npm install
npm run build
```
