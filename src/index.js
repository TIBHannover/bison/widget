'use strict';

(function () {

  function setupForm () {
    const widget = document.getElementById("bison-widget");

    if (widget === null) {
      console.log("div with id 'bison-widget' not found");
      return;
    }

    const searchMask = `
      Try B!SON's open access journal recommendation here. For advanced filtering and further 
      details visit: <a href="https://service.tib.eu/bison" target="_blank">service.tib.eu/bison</a>
      <form id="bison-form">
        <label for="bison-title">Title:</label>
        <br/>
        <input type="text" id="bison-title" />
        <br/>
        <label for="bison-abstract">Abstract:</label>
        <br/>
        <textarea id="bison-abstract" rows="3"></textarea>
        <br/>
        <label for="bison-references">References:</label>
        <br/>
        <textarea id="bison-references" rows="3"></textarea>
        <div>
          <button id="bison-button" type="button">Search for journals</button>
        </div>
      </form>
      <div id="bison-results"></div>
    `;

    widget.innerHTML = searchMask;
    document.getElementById('bison-button').onclick = search;

    // create CSS
    const head = document.getElementsByTagName('head')[0];
    const link = document.createElement('link');
    link.rel = 'stylesheet';
    link.href = 'data:text/css,' + escape(`
      #bison-widget {
        font-family: sans-serif;
        max-width: 500px;
      }
      #bison-widget a {
        text-decoration: none;
        color: rgb(13, 110, 253);
      }
      #bison-form {
        margin-top: 10px;
      }
      #bison-form > input, #bison-form > textarea {
        border-radius: 6px;
        border: 1px solid #bfbfbf;
        width: calc(100% - 5px);
      }
      #bison-results > div {
        border: 1px solid #bfbfbf;
        padding: 5px 10px;
        margin: 5px 0;
        border-radius: 4px;
        display: flex;
        gap: 10px;
      }
      #bison-button {
        margin: 5px 0;
        color: white;
        background-color: rgb(13, 110, 253);
        font-size: 16px;
        padding: 0.375rem 0.75rem;
        border: 1px solid rgb(13, 110, 253);
        border-radius: 6px;
      }
      .bison-score {
        min-width: 35px;
      }
    `);
    head.appendChild(link);

  }

  /* Show a loading text while the request is sent */
  function createLoadingAnimation () {
    const results_container = document.getElementById('bison-results');
    results_container.innerHTML = 'Loading...';
  };

  /* Create one journal entry in the result list */
  function createResultEntry (container, journal) {
    const entry = document.createElement("div");
    entry.innerHTML = `
      <div class="bison-score">${Math.round(journal.score.value * 100)}%</div>
      <div>
        <a href="https://service.tib.eu/bison/journal/${journal.idx}" target="_blank">${journal.title}</a>
        <br / >
        Publisher: ${journal.publisher_name}
      </div>
    `;
    container.appendChild(entry);
  };

  /* Display the results of a search */
  function showResults (journals) {
    const results_container = document.getElementById('bison-results');
    results_container.innerHTML = 'Results:';
    if (journals.length > 0) {
      for (let journal of journals.slice(0, 15)) {
        createResultEntry(results_container, journal);
      }
    } else {
      results_container.innerHTML = 'No results';
    }
  }

  function search () {
    // send request
    createLoadingAnimation();
    const title = document.getElementById('bison-title').value;
    const abstract = document.getElementById('bison-abstract').value;
    const references = document.getElementById('bison-references').value;
    fetch('https://service.tib.eu/bison/api/public/v1/search', {
      method: 'Post',
      body: JSON.stringify({
        title: title,
        abstract: abstract,
        references: references
      }),
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'User-Agent': 'bison-widget'
      })
    }).then(response => response.json())
    .then(data => {
      showResults(data.journals);
    }).catch((error) => {
        document.getElementById('bison-results').innerHTML = 'Error fetching results';
        console.log('B!SON ERROR: ' + error);
    });
  }


  // directly try to set up form and again after page has loaded
  setupForm();
  addEventListener('DOMContentLoaded', (event) => {setupForm()});

}) ();
